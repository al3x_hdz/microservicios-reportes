# App Reportería (Backend)
Proyecto desarrollado por el equipo de Angular, ofrece los microservicios (API's) que rescatan información 
de la base de datos Trigarante2020 y los expone para ser consultados por el frontend.

## Documentación
Para ver la documentación detallada del proyecto click [aquí](https://drive.google.com/drive/folders/1q6qHoEZsMtWQQ6JVevQq44ZLZ5neJ_vg?usp=sharing)

### Proyectos relacionados
* [ReportesApp (frontend)](https://bitbucket.org/al3x_hdz/reportesapp/src/master/)
* [Tickets](https://bitbucket.org/trigarante/workspace/projects/TIC)

### Carpeta de poryecto completo en Bitbucket
* [Reportes](https://bitbucket.org/al3x_hdz/workspace/projects/REP)


## El proyecto cuenta con las siguientes características:


* [Node.js](https://nodejs.org/es/) v16.6.2
* [Typescript](https://www.npmjs.com/package/typescript)
* [Express.js](https://www.npmjs.com/package/express)
* [Dotenv](https://www.npmjs.com/package/dotenv) para variables de entorno
* [Sequelize ORM](https://sequelize.org/master/) V6
* [mysql2](https://www.npmjs.com/package/mysql2)
* [google-auth-library](https://www.npmjs.com/package/google-auth-library) (verificacion de token google)
* [Nodemon](https://www.npmjs.com/package/nodemon)

## Instrucciones de uso
1. Para que el proyecto funcione primero debemos llenar el archivo [.env](https://bitbucket.org/al3x_hdz/microservicios-reportes/src/master/.env), el cual debe contener la configuración para conectarse a la base de datos Trigarante2020.
2. para entorno de desarrollo que se auto actualiza, usar el comando: ```
    npm run dev
    ```
3. Para entorno de produccion usar los siguientes comandos:
```
tsc
npm run start
```

## Endpoints
El proyecto cuenta con varios endpoints, algunos de estos requieren parametros.
Antes de modificar recomiendo probarlos primero con [Insomnia](https://insomnia.rest/) o [Postman](https://www.postman.com/).

* route -----> /usuarios/google/login (POST) |En el body se recibe "token" que es un string con el token google
* route -----> /llamadas-entrantes (GET) |rescata todas las entradas de la vista "llamadasEnT2020"
* route -----> /llamadas-entrantes/:month/:year (GET) |recibe mes y año, rescata las entradas "llamadasEnT2020" que coincidan 

* route -----> /venta-nueva/:fechaIn/:fechaFin (GET) |recibe rango de fechas y retorna: { boardVentaNueva, boardGMM, boardPeru,tabla(cam x cia), subtotal, totalGeneral, campannas } 



