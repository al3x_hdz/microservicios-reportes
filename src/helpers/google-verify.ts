import { OAuth2Client } from "google-auth-library";
import dotenv from "dotenv";

dotenv.config();

const client = new OAuth2Client(process.env.GOOGLE_ID);

export const googleVerify = async (token: string) => {
  try {
    const ticket: any = await client.verifyIdToken({
      idToken: token,
      audience: process.env.GOOGLE_ID,
    });

    const email = ticket.payload.email;
    const picture = ticket.payload.picture;
    return { email, picture };
  } catch (error) {
    console.log(error);
    return null;
  }
};
