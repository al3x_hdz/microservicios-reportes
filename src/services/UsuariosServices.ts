import Usuario from "../database/models/Usuario";

export default class UsuariosService {
  static async getUser(email: string) {
    try {
      return await Usuario.findOne({
        where: {
          usuario: email,
        },
      });
    } catch (error) {
      console.log(error);
      return null;
    }
  }
}
