import { sequelize } from "../database/db";

export default class VentaNuevaServices {
  static async getContactos(fechaIn: string, fechaFin: string) {
    try {
      return await sequelize.query(
        "CALL trigarante2020.ContactosConConversionConFiltros(str_to_date('" +
          fechaIn +
          "', '%Y-%m-%d'),str_to_date('" +
          fechaFin +
          "', '%Y-%m-%d'))"
      );
    } catch (error) {
      return error;
    }
  }

  static async getProdDiaria(fechaIn: string, fechaFin: string) {
    try {
      return await sequelize.query(
        "CALL trigarante2020.productividadxDia(str_to_date('" +
          fechaIn +
          "', '%Y-%m-%d'),str_to_date('" +
          fechaFin +
          "', '%Y-%m-%d'))"
      );
    } catch (error) {
      return error;
    }
  }

  static async getBaseLlamadasEnt(fechaIn: string, fechaFin: string) {
    try {
      return await sequelize.query(
        "CALL trigarante2020.LlamadasEntrantesPjsipOpcionesIvr(str_to_date('" +
          fechaIn +
          "', '%Y-%m-%d'),str_to_date('" +
          fechaFin +
          "', '%Y-%m-%d'),str_to_date('" +
          fechaIn +
          "', '%Y-%m-%d'),str_to_date('" +
          fechaFin +
          "', '%Y-%m-%d'))"
      );
    } catch (error) {
      return error;
    }
  }
}
