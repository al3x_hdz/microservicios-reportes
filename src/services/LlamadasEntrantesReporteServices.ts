import { QueryTypes } from "sequelize";
import { sequelize } from "../database/db";

import LLamadasEntrantes from "../database/models/LLamadasEntrantes";

export default class LLamadasEntrantesService {
  /**
   * Looks for all the rows inside llamadasEntranT2020 table/view
   *
   * @returns llamadas - array with the results
   */
  static async getAll() {
    try {
      return await LLamadasEntrantes.findAll();
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  /**
   *Looks for all the rows inside llamadasEntranT2020 table/view
   *where the status is 'Abandonada' or 'Contestada'
   *
   * @returns llamadas - array with the results
   */
  static async getByMonth(month: number, year: number) {
    try {
      return await sequelize.query(
        `
        select
          *
        from
        llamadasEnT2020
        where
          fechaGen like '${year}-%${month}-%'
      `,
        { type: QueryTypes.SELECT }
      );
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  /**
   * Looks for rows inside llamadasEntranT2020 table/view
   * where nombreEmpleado is equal param nombre
   *
   * @param nombre - string with the name we are going to look
   * @returns llamadas - array with the results
   */
  static async getByName(nombre: string) {
    try {
      return await sequelize.query(
        `
        select
          *
        from
          llamadasEntranT2020
        where nombreEmpleado = '${nombre}'
          and calldate is not null          
        order by calldate
      `,
        { type: QueryTypes.SELECT }
      );
    } catch (error) {
      console.log(error);
      return error;
    }
  }
}
