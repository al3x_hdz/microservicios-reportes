import { Board } from "../../database/models/ventaNueva/Board";
import { Campanna } from "../../database/models/ventaNueva/Campanna";

let board: Board = {
  outbound: {
    leads: 0,
    npc: 0,
    conversion: 0,
  },
  interaccionOnline: {
    leads: 0,
    npc: 0,
    conversion: 0,
  },
  interaccionTelefonica: {
    leads: 0,
    npc: 0,
    conversion: 0,
  },
  inbound: {
    leads: 0,
    npc: 0,
    conversion: 0,
  },
  otros: {
    leads: 0,
    npc: 0,
    conversion: 0,
  },
  mejorPrecio: {
    leads: 0,
    npc: 0,
    conversion: 0,
  },
  cobranza: {
    leads: 0,
    npc: 0,
    conversion: 0,
  },
  retencion: {
    leads: 0,
    npc: 0,
    conversion: 0,
  },
};

/**
 * Procesa las campañas pertenecientes a Venta Nueva
 * @param campannas Campannas[]
 * @returns Campannas[]
 */
export function procesaDatosVN(campannas: Campanna[]) {
  reiniciaBoard();

  campannas.map((campanna) => {
    procesaOtbound(campanna);
    procesaInteraccionesOnline(campanna);
    procesaInteraccionesTelefonica(campanna);
    procesaInbound(campanna);
    procesaOtros(campanna);
    generaMejorPrecio(campanna);
    generaCobranza(campanna);
    generaRetencion(campanna);
  });

  return board;
}

/**
 * SI la campanña.tipoCOntacto pertenece a Outbound, extraemos sus
 * leads, npc y convesion.
 * @param campanna: Camppanna
 */
let procesaOtbound = (campanna: Campanna) => {
  if (campanna.tipoContacto === "OUTBOUND") {
    board.outbound.leads += campanna.contactosProd;
    board.outbound.npc += campanna.npc;
    board.outbound.conversion = calculaConversion(
      board.outbound.leads,
      board.outbound.npc
    );
  }
};

/**
 * SI la campanña.tipoContacto pertenece a INTERACCION ONLINE, extraemos sus
 * leads, npc y convesion.
 * @param campanna: Camppanna
 */
const procesaInteraccionesOnline = (campanna: Campanna) => {
  if (campanna.tipoContacto === "INTERACCION ONLINE") {
    board.interaccionOnline.leads += campanna.contactosProd;
    board.interaccionOnline.npc += campanna.npc;
    board.interaccionOnline.conversion = calculaConversion(
      board.interaccionOnline.leads,
      board.interaccionOnline.npc
    );
  }
};

/**
 * SI la campanña.tipoContacto pertenece a INTERACCION TELEFONICA, extraemos sus
 * leads, npc y convesion.
 * @param campanna: Camppanna
 */
const procesaInteraccionesTelefonica = (campanna: Campanna) => {
  if (campanna.tipoContacto === "INTERACCION TELEFONICA") {
    board.interaccionTelefonica.leads += campanna.contactosProd;
    board.interaccionTelefonica.npc += campanna.npc;
    board.interaccionTelefonica.conversion = calculaConversion(
      board.interaccionTelefonica.leads,
      board.interaccionTelefonica.npc
    );
  }
};

/**
 * SI la campanña.tipoContacto pertenece a IBOUND, extraemos sus
 * leads, npc y convesion.
 * @param campanna: Camppanna
 */
const procesaInbound = (campanna: Campanna) => {
  if (campanna.tipoContacto === "INBOUND") {
    board.inbound.leads += campanna.contactosProd;
    board.inbound.npc += campanna.npc;
    board.inbound.conversion = calculaConversion(
      board.inbound.leads,
      board.inbound.npc
    );
  }
};

/**
 * SI la campanña.tipoContacto pertenece a OTROS, extraemos sus
 * leads, npc y convesion.
 * @param campanna: Camppanna
 */
const procesaOtros = (campanna: Campanna) => {
  if (campanna.tipoContacto === "OTROS" && campanna.tipoSubarea != null) {
    board.otros.leads += campanna.contactosProd;
    board.otros.npc += campanna.npc;
    board.otros.conversion = calculaConversion(
      board.otros.leads,
      board.otros.npc
    );
  }
};

/**
 * Calculamos el mejor precion sumando datos de outbound,
 * interaccion online, interaccion telefonica.
 * Calculamos conversión despues de eso
 * @param campanna Campanna
 */
const generaMejorPrecio = (campanna: Campanna) => {
  //leads = (la suma de leads de outbound, int tel, int onl) - (la suma de npc de outbound, int tel, int onl)
  board.mejorPrecio.leads =
    board.outbound.leads +
    board.interaccionOnline.leads +
    board.interaccionTelefonica.leads -
    (board.outbound.npc +
      board.interaccionOnline.npc +
      board.interaccionTelefonica.npc);
  if (campanna.tipoContacto === "MEJOR PRECIO") {
    board.mejorPrecio.npc += campanna.npc;

    board.mejorPrecio.conversion = calculaConversion(
      board.mejorPrecio.leads,
      board.mejorPrecio.npc
    );
  }
};

/**
 * los leads de cobranza son iguales a los de Mejor precio,
 * despueés si la campanna.tipoContacto es igual a COBRANZA
 * añadimos sus npc. Por finalizar calculamos conversión
 * @param campanna Campanna
 */
const generaCobranza = (campanna: Campanna) => {
  board.cobranza.leads = board.mejorPrecio.leads;

  if (campanna.tipoContacto === "COBRANZA") {
    board.cobranza.npc += campanna.npc;
    board.cobranza.conversion = calculaConversion(
      board.cobranza.leads,
      board.cobranza.npc
    );
  }
};

/**
 * los leads de cobranza son iguales a los de Mejor precio,
 * despueés si la campanna.tipoContacto es igual a COBRANZA
 * añadimos sus npc. Por finalizar calculamos conversión
 * @param campanna Campanna
 */
const generaRetencion = (campanna: Campanna) => {
  board.retencion.leads = board.mejorPrecio.leads;
  if (campanna.tipoContacto === "RETENCION") {
    board.retencion.npc += campanna.npc;
    board.retencion.conversion = calculaConversion(
      board.retencion.leads,
      board.retencion.npc
    );
  }
};

//Reinicia e inicializa las variables a 0
const reiniciaBoard = () => {
  board = {
    outbound: {
      leads: 0,
      npc: 0,
      conversion: 0,
    },
    interaccionOnline: {
      leads: 0,
      npc: 0,
      conversion: 0,
    },
    interaccionTelefonica: {
      leads: 0,
      npc: 0,
      conversion: 0,
    },
    inbound: {
      leads: 0,
      npc: 0,
      conversion: 0,
    },
    otros: {
      leads: 0,
      npc: 0,
      conversion: 0,
    },
    mejorPrecio: {
      leads: 0,
      npc: 0,
      conversion: 0,
    },
    cobranza: {
      leads: 0,
      npc: 0,
      conversion: 0,
    },
    retencion: {
      leads: 0,
      npc: 0,
      conversion: 0,
    },
  };
};

const calculaConversion = (contactosProd: number, npc: number) => {
  if (contactosProd > 0) {
    return (npc * 100) / contactosProd;
  } else {
    return 0;
  }
};
