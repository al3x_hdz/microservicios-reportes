import { Board } from "../../database/models/ventaNueva/Board";
import { Campanna } from "../../database/models/ventaNueva/Campanna";

let board: Board = {
  outbound: {
    leads: 0,
    npc: 0,
    conversion: 0,
  },
  interaccionOnline: {
    leads: 0,
    npc: 0,
    conversion: 0,
  },
  interaccionTelefonica: {
    leads: 0,
    npc: 0,
    conversion: 0,
  },
  inbound: {
    leads: 0,
    npc: 0,
    conversion: 0,
  },
  otros: {
    leads: 0,
    npc: 0,
    conversion: 0,
  },
  mejorPrecio: {
    leads: 0,
    npc: 0,
    conversion: 0,
  },
  cobranza: {
    leads: 0,
    npc: 0,
    conversion: 0,
  },
  retencion: {
    leads: 0,
    npc: 0,
    conversion: 0,
  },
};

/**
 * Procesa las campañas pertenecientes a PERU
 * @param campannas Campannas[]
 * @returns Campannas[]
 */
export function procesaDatosPeru(campannas: Campanna[]) {
  reiniciaBoard();

  campannas.map((campanna) => {
    procesaOtbound(campanna);
    procesaInbound(campanna);
    generaMejorPrecio(campanna);
    generaCobranza(campanna);
    generaRetencion(campanna);
  });

  return board;
}

/**
 * SI la campanña pertenece a Peru, extraemos sus
 * leads, npc y convesion.
 * @param campanna: Camppanna
 */
const procesaOtbound = (campanna: Campanna) => {
  if (campanna.tipoContacto === "PERU") {
    board.outbound.leads += campanna.contactosProd;
    board.outbound.npc += campanna.npc;
    board.outbound.conversion = calculaConversion(
      board.outbound.leads,
      board.outbound.npc
    );
  }
};

const procesaInbound = (campanna: Campanna) => {
  if (campanna.tipoContacto === "INBOUND") {
    board.inbound.leads += campanna.contactosProd;
    board.inbound.npc += campanna.npc;
    board.inbound.conversion = calculaConversion(
      board.inbound.leads,
      board.inbound.npc
    );
  }
};

/**
 * Calculamos el mejor precion sumando datos de outbound,
 * interaccion online, interaccion telefonica.
 * Calculamos conversión despues de eso
 * @param campanna Campanna
 */
const generaMejorPrecio = (campanna: Campanna) => {
  board.mejorPrecio.leads =
    board.outbound.leads +
    board.interaccionOnline.leads +
    board.interaccionTelefonica.leads -
    (board.outbound.npc +
      board.interaccionOnline.npc +
      board.interaccionTelefonica.npc);
  if (campanna.tipoContacto === "MEJOR PRECIO") {
    board.mejorPrecio.npc += campanna.npc;

    board.mejorPrecio.conversion = calculaConversion(
      board.mejorPrecio.leads,
      board.mejorPrecio.npc
    );
  }
};

/**
 * los leads de cobranza son iguales a los de Mejor precio,
 * despueés si la campanna.tipoContacto es igual a COBRANZA
 * añadimos sus npc. Por finalizar calculamos conversión
 * @param campanna Campanna
 */
const generaCobranza = (campanna: Campanna) => {
  board.cobranza.leads = board.mejorPrecio.leads;

  if (campanna.tipoContacto === "COBRANZA") {
    board.cobranza.npc += campanna.npc;
    board.cobranza.conversion =
      board.cobranza.npc > 0 && board.cobranza.leads < 10
        ? calculaConversion(board.cobranza.leads, board.cobranza.npc)
        : calculaConversion(board.cobranza.leads, board.cobranza.npc);
  }
};

/**
 * los leads de cobranza son iguales a los de Mejor precio,
 * despueés si la campanna.tipoContacto es igual a COBRANZA
 * añadimos sus npc. Por finalizar calculamos conversión
 * @param campanna Campanna
 */
const generaRetencion = (campanna: Campanna) => {
  board.retencion.leads = board.mejorPrecio.leads;
  if (campanna.tipoContacto === "RETENCION") {
    board.retencion.npc += campanna.npc;
    board.retencion.conversion = calculaConversion(
      board.retencion.leads,
      board.retencion.npc
    );
  }
};

/**
 * Reinicia el contenido del board con valores en 0
 */
const reiniciaBoard = () => {
  board = {
    outbound: {
      leads: 0,
      npc: 0,
      conversion: 0,
    },
    interaccionOnline: {
      leads: 0,
      npc: 0,
      conversion: 0,
    },
    interaccionTelefonica: {
      leads: 0,
      npc: 0,
      conversion: 0,
    },
    inbound: {
      leads: 0,
      npc: 0,
      conversion: 0,
    },
    otros: {
      leads: 0,
      npc: 0,
      conversion: 0,
    },
    mejorPrecio: {
      leads: 0,
      npc: 0,
      conversion: 0,
    },
    cobranza: {
      leads: 0,
      npc: 0,
      conversion: 0,
    },
    retencion: {
      leads: 0,
      npc: 0,
      conversion: 0,
    },
  };
};

const calculaConversion = (contactosProd: number, npc: number) => {
  if (contactosProd > 0) {
    return (npc * 100) / contactosProd;
  } else {
    return 0;
  }
};
