import { Campanna } from "../../database/models/ventaNueva/Campanna";
import { camXCiaRow } from "../../database/models/ventaNueva/CamXCia";

//filas correspondientes a campanaGral
const filas = [
  "ABA",
  "AFIRME",
  "ANA",
  "ATLAS",
  "BANORTE",
  "EL AGUILA",
  "EL POTOSI",
  "GENERAL DE SEGUROS",
  "GNP BRAND",
  "HDI",
  "INBURSA",
  "LA LATINO",
  "MAPFRE",
  "MIGO",
  "MOTOS QUALITAS",
  "MULTIMARCAS",
  "QUALITAS BRAND",
  "SURA",
  "UBER QUALITAS",
  "TOTAL MARCAS",
  "AHORRA",
  "COMPARA YA",
  "MOTOS",
  "TAXIS",
  "TRACTOCAMIONES",
  "TOTAL COMPARADORES",
  "TOTAL VENTA NUEVA",
  "MEJOR PRECIO",
  "GMM",
  "PERU",
  "OTROS",
  "TOTAL",
  "COBRANZA",
  "RETENCION",
  "TOTAL GENERAL",
];

const marcas = [
  "ABA",
  "AFIRME",
  "ATLAS",
  "BANORTE",
  "EL AGUILA",
  "EL POTOSI",
  "GENERAL DE SEGUROS",
  "GNP BRAND",
  "HDI",
  "INBURSA",
  "LA LATINO",
  "MAPFRE",
  "MIGO",
  "MOTOS QUALITAS",
  "MULTIMARCAS",
  "QUALITAS BRAND",
  "SURA",
  "UBER QUÁLITAS",
  "TOTAL MARCAS",
];

const comparadores = [
  "AHORRA",
  "COMPARA YA",
  "MOTOS",
  "TAXIS",
  "TRACTOCAMIONES",
  "UBER",
  "TOTAL COMPARADORES",
  "TOTAL VENTA NUEVA",
];

const mejorPrecio = ["MEJOR PRECIO"];

let camXCia: camXCiaRow[] = [];

/**
 * Se genera la tabla correspondiente a CamxCia
 * @param campannas tabla campannas
 * @param gmm subtotal de Gastom Medicos
 * @param peru  subtotal ed Peru
 * @param cobranza de boardVentaNueva
 * @param retencion de boardVentaNueva
 * @returns CamxCia: camXCiaRow[]
 */
export function generaTablaCamxCia(
  campannas: Campanna[],
  gmm: any,
  peru: any,
  cobranza: any,
  retencion: any
) {
  reiniciaVariable();
  //iteramos por cada campaña para procesar su información
  campannas.map((campanna) => {
    insertAlas(campanna);
    insertAna(campanna);
    inserUberQualitas(campanna);
    generaCamXCia(campanna, marcas);
    generaCamXCia(campanna, comparadores);
    generaMejoresPrecios(campanna, mejorPrecio);
    generaGMMPeru(campanna, gmm, peru);
    generaOtros(campanna);
    generaCobranzaRetencion(campanna, cobranza, retencion);
    insertCompara(campanna);
  });

  /**
   *una vez llenadas las filas y columnas podemos calcular datos generales o totales
   */

  calculaTotalMarcas();
  calculaTotalComparadores();
  calculaTotalVentaNueva();
  calculaTotal();
  calculaTotalGeneral();

  return camXCia;
}

/**
 * iteramos en cada entrada de conjunto recibido y lo buscamos en
 * camxCia, si la campanna recibida coincide en campanna.compannia,
 * entonces separamos por tipoContacto y se inserta en la
 * fila de la tabla.
 *
 * No se toman en cuenta ANA ni ATLAS por que su proceso
 * es diferente
 * @param campanna: Campanna
 * @param conjunto: string[]
 */
const generaCamXCia = (campanna: Campanna, conjunto: string[]) => {
  conjunto.map((iterador) => {
    if (campanna.compannia !== "ANA" && campanna.compannia !== "ATLAS") {
      //Buscamos indice del iterador de conjunto
      let index = camXCia.map((row) => row.campanaGral).indexOf(iterador);
      //Si no se encuentra iterator index = -1
      if (campanna.compannia === iterador && index !== -1) {
        switch (campanna.tipoContacto) {
          case "OUTBOUND":
            camXCia[index].outLead += campanna.contactosProd;
            camXCia[index].outNpc += campanna.npc;
            camXCia[index].outConv = trunc(
              calculaConversion(camXCia[index].outLead, camXCia[index].outNpc)
            );
            camXCia[index].pCobrada += campanna.pCobrada;

            break;

          case "INTERACCION TELEFONICA":
            camXCia[index].telLead += campanna.contactosProd;
            camXCia[index].telNpc += campanna.npc;
            camXCia[index].telConv = trunc(
              calculaConversion(camXCia[index].telLead, camXCia[index].telNpc)
            );
            camXCia[index].pCobrada += campanna.pCobrada;
            break;

          case "INBOUND":
            camXCia[index].inLead += campanna.contactosProd;
            camXCia[index].inNpc += campanna.npc;
            camXCia[index].inConv = trunc(
              calculaConversion(camXCia[index].inLead, camXCia[index].inNpc)
            );
            camXCia[index].pCobrada += campanna.pCobrada;
            break;

          case "INTERACCION ONLINE":
            camXCia[index].onlLead += campanna.contactosProd;
            camXCia[index].onlNpc += campanna.npc;
            camXCia[index].onlConv = trunc(
              calculaConversion(camXCia[index].onlLead, camXCia[index].onlNpc)
            );
            camXCia[index].pCobrada += campanna.pCobrada;
            break;
          default:
            break;
        }
        //A cada entrada le asignamos los valores generales
        camXCia[index].gralLead =
          camXCia[index].outLead +
          camXCia[index].telLead +
          camXCia[index].onlLead;
        camXCia[index].gralNpc =
          camXCia[index].outNpc + camXCia[index].telNpc + camXCia[index].onlNpc;
        camXCia[index].gralConv = trunc(
          calculaConversion(camXCia[index].gralLead, camXCia[index].gralNpc)
        );
      }
    }
  });
};

/**
 * si la campanna.compannia es UBER QUALITAS
 * agrega sus datos a la tabla
 * @param campanna: Campanna
 */
const inserUberQualitas = (campanna: Campanna) => {
  if (campanna.compannia === "UBER QUALITAS") {
    let index = camXCia.map((row) => row.campanaGral).indexOf("UBER QUALITAS");
    camXCia[index].outLead += campanna.contactosProd;
    camXCia[index].outNpc += campanna.npc;
    camXCia[index].outConv = trunc(
      calculaConversion(camXCia[index].outLead, camXCia[index].outNpc)
    );
    camXCia[index].pCobrada += campanna.pCobrada;

    camXCia[index].gralLead =
      camXCia[index].outLead + camXCia[index].telLead + camXCia[index].onlLead;
    camXCia[index].gralNpc =
      camXCia[index].outNpc + camXCia[index].telNpc + camXCia[index].onlNpc;
    camXCia[index].gralConv = trunc(
      calculaConversion(camXCia[index].gralLead, camXCia[index].gralNpc)
    );
  }
};

/**
 * Si campanna.compannia es ATLAS,
 * insertamos datos en la tabla
 * @param campanna: Campanna
 */
const insertAlas = (campanna: Campanna) => {
  if (campanna.compannia === "ATLAS") {
    let index = camXCia.map((row) => row.campanaGral).indexOf("ATLAS");
    camXCia[index].outLead += campanna.contactosProd;
    camXCia[index].outNpc += campanna.npc;
    camXCia[index].outConv = trunc(
      calculaConversion(camXCia[index].outLead, camXCia[index].outNpc)
    );
    camXCia[index].pCobrada += campanna.pCobrada;

    camXCia[index].gralLead =
      camXCia[index].outLead + camXCia[index].telLead + camXCia[index].onlLead;
    camXCia[index].gralNpc =
      camXCia[index].outNpc + camXCia[index].telNpc + camXCia[index].onlNpc;
    camXCia[index].gralConv = trunc(
      calculaConversion(camXCia[index].gralLead, camXCia[index].gralNpc)
    );
  }
};

/**
 * Si campanna.compannia es ANA,
 * insertamos datos en la tabla
 * @param campanna: Campanna
 */
const insertAna = (campanna: Campanna) => {
  if (campanna.compannia === "ANA") {
    let index = camXCia.map((row) => row.campanaGral).indexOf("ANA");
    camXCia[index].outLead += campanna.contactosProd;
    camXCia[index].outNpc += campanna.npc;
    camXCia[index].outConv = trunc(
      calculaConversion(camXCia[index].outLead, camXCia[index].outNpc)
    );
    camXCia[index].pCobrada += campanna.pCobrada;

    camXCia[index].gralLead =
      camXCia[index].outLead + camXCia[index].telLead + camXCia[index].onlLead;
    camXCia[index].gralNpc =
      camXCia[index].outNpc + camXCia[index].telNpc + camXCia[index].onlNpc;
    camXCia[index].gralConv = trunc(
      calculaConversion(camXCia[index].gralLead, camXCia[index].gralNpc)
    );
  }
};

/**
 * Si campanna.compannia es COMPARA,
 * insertamos datos en la tabla
 * @param campanna: Campanna
 */
const insertCompara = (campanna: Campanna) => {
  if (campanna.compannia === "COMPARA YA") {
    let index = camXCia.map((row) => row.campanaGral).indexOf("COMPARA YA");
    camXCia[index].gralLead =
      camXCia[index].outLead +
      camXCia[index].inLead +
      camXCia[index].telLead +
      camXCia[index].onlLead;
    camXCia[index].gralNpc =
      camXCia[index].outNpc +
      camXCia[index].inNpc +
      camXCia[index].telNpc +
      camXCia[index].onlNpc;
    camXCia[index].gralConv = trunc(
      calculaConversion(camXCia[index].gralLead, camXCia[index].gralNpc)
    );
  }
};

/**
 * Si campanna.compannia es OTROS,
 * insertamos datos en la tabla
 * @param campanna: Campanna
 */
const generaOtros = (campanna: Campanna) => {
  let otrosIndex = camXCia.map((row) => row.campanaGral).indexOf("OTROS");
  if (campanna.tipoContacto === "OTROS") {
    camXCia[otrosIndex].gralLead += campanna.contactosProd;
    camXCia[otrosIndex].gralNpc += campanna.npc;
    camXCia[otrosIndex].gralConv = trunc(
      calculaConversion(
        camXCia[otrosIndex].gralLead,
        camXCia[otrosIndex].gralNpc
      )
    );
    camXCia[otrosIndex].pCobrada += campanna.pCobrada;
  }
};

/**
 * Si campanna.compannia es MEJOR PRECIO,
 * insertamos datos en la tabla
 * @param campanna: Campanna
 */
const generaMejoresPrecios = (campanna: Campanna, conjunto: string[]) => {
  conjunto.map((iterador) => {
    let index = camXCia.map((row) => row.campanaGral).indexOf(iterador);
    if (campanna.compannia === iterador && index !== -1) {
      //A cada entrada le asignamos los valores generales
      camXCia[index].gralLead += campanna.contactosProd;
      camXCia[index].gralNpc = campanna.npc;
      camXCia[index].gralConv = trunc(
        calculaConversion(camXCia[index].gralLead, camXCia[index].gralNpc)
      );
      camXCia[index].pCobrada += campanna.pCobrada;
    }
  });
};

/**
 * A partir del subtotal, vamos a llenar los datos de peru y GMM
 * @param campanna: Campanna
 * @param gmmSubtotal {leads, ventas, conversion}
 * @param peruSubtotal {leads, ventas, conversion}
 */
const generaGMMPeru = (
  campanna: Campanna,
  gmmSubtotal: any,
  peruSubtotal: any
) => {
  let gmmIndex = camXCia.map((row) => row.campanaGral).indexOf("GMM");

  let peruIndex = camXCia.map((row) => row.campanaGral).indexOf("PERU");

  camXCia[gmmIndex].gralLead = gmmSubtotal.leads;
  camXCia[gmmIndex].gralNpc = gmmSubtotal.ventas;
  camXCia[gmmIndex].gralConv = gmmSubtotal.conversion;

  camXCia[peruIndex].gralLead = peruSubtotal.leads;
  camXCia[peruIndex].gralNpc = peruSubtotal.ventas;
  camXCia[peruIndex].gralConv = peruSubtotal.conversion;

  if (campanna.compannia === "GMM") {
    camXCia[gmmIndex].pCobrada += campanna.pCobrada;
  }

  if (campanna.compannia === "PERU") {
    camXCia[peruIndex].pCobrada += campanna.pCobrada;
  }
};

/**
 *
 * @param campanna: Campanna
 * @param cobranza: arreglo de objetos [{leads, ventas, conversion} ]
 * @param retencion arreglo de objetos [{leads, ventas, conversion} ]
 */
const generaCobranzaRetencion = (
  campanna: Campanna,
  cobranza: any,
  retencion: any
) => {
  let cobranzaIndex = camXCia.map((row) => row.campanaGral).indexOf("COBRANZA");
  let retencionIndex = camXCia
    .map((row) => row.campanaGral)
    .indexOf("RETENCION");

  camXCia[cobranzaIndex].gralLead += cobranza.npc;
  camXCia[cobranzaIndex].gralNpc += cobranza.npc;
  camXCia[cobranzaIndex].gralConv = trunc(
    calculaConversion(
      camXCia[cobranzaIndex].gralLead,
      camXCia[cobranzaIndex].gralNpc
    )
  );

  camXCia[retencionIndex].gralLead += retencion.npc;
  camXCia[retencionIndex].gralNpc += retencion.npc;
  camXCia[retencionIndex].gralConv = trunc(
    calculaConversion(
      camXCia[retencionIndex].gralLead,
      camXCia[retencionIndex].gralNpc
    )
  );

  if (campanna.compannia === "COBRANZA") {
    camXCia[cobranzaIndex].pCobrada += campanna.pCobrada;
  }

  if (campanna.compannia === "RETENCION") {
    camXCia[retencionIndex].pCobrada += campanna.pCobrada;
  }
};

/**
 * Se suman todas las columnas que hay desde la fila 1 hasta la fila 21 de la tabla,
 * una vez sumadas podemos sacar los valores generales
 */
const calculaTotalMarcas = () => {
  let totalMarcasIndex = camXCia
    .map((row) => row.campanaGral)
    .indexOf("TOTAL MARCAS");

  for (let i = 0; i < totalMarcasIndex - 1; i++) {
    ///OUTBOUND
    camXCia[totalMarcasIndex].outLead += camXCia[i].outLead;
    camXCia[totalMarcasIndex].outNpc += camXCia[i].outNpc;
    camXCia[totalMarcasIndex].outConv = trunc(
      calculaConversion(
        camXCia[totalMarcasIndex].outLead,
        camXCia[totalMarcasIndex].outNpc
      )
    );

    //INTERACCION TELEFONICA
    camXCia[totalMarcasIndex].telLead += camXCia[i].telLead;
    camXCia[totalMarcasIndex].telNpc += camXCia[i].telNpc;
    camXCia[totalMarcasIndex].telConv = trunc(
      calculaConversion(
        camXCia[totalMarcasIndex].telLead,
        camXCia[totalMarcasIndex].telNpc
      )
    );

    ////INBOUND
    camXCia[totalMarcasIndex].inLead += camXCia[i].inLead;
    camXCia[totalMarcasIndex].inNpc += camXCia[i].inNpc;
    camXCia[totalMarcasIndex].inConv = trunc(
      calculaConversion(
        camXCia[totalMarcasIndex].inLead,
        camXCia[totalMarcasIndex].inNpc
      )
    );

    ////INTERACCION ONLINE
    camXCia[totalMarcasIndex].onlLead += camXCia[i].onlLead;
    camXCia[totalMarcasIndex].onlNpc += camXCia[i].onlNpc;
    camXCia[totalMarcasIndex].onlConv = trunc(
      calculaConversion(
        camXCia[totalMarcasIndex].onlLead,
        camXCia[totalMarcasIndex].onlNpc
      )
    );

    /////POLIZA COBRADA
    camXCia[totalMarcasIndex].pCobrada += camXCia[i].pCobrada;
  }

  camXCia[totalMarcasIndex].pCobrada = trunc(
    camXCia[totalMarcasIndex].pCobrada,
    2
  );
  //A cada entrada le asignamos los valores generales
  camXCia[totalMarcasIndex].gralLead =
    camXCia[totalMarcasIndex].outLead +
    camXCia[totalMarcasIndex].telLead +
    camXCia[totalMarcasIndex].onlLead;
  camXCia[totalMarcasIndex].gralNpc =
    camXCia[totalMarcasIndex].outNpc +
    camXCia[totalMarcasIndex].telNpc +
    camXCia[totalMarcasIndex].onlNpc;
  camXCia[totalMarcasIndex].gralConv = trunc(
    calculaConversion(
      camXCia[totalMarcasIndex].gralLead,
      camXCia[totalMarcasIndex].gralNpc
    )
  );
};

/**
 * Se suman todas las columnas que hay desde la fila 22 hasta la 27 de la tabla,
 * despues de la suma podemos sacar los valorres generales
 */
const calculaTotalComparadores = () => {
  let totalComparadoresIndex = camXCia
    .map((row) => row.campanaGral)
    .indexOf("TOTAL COMPARADORES");

  for (let i = 20; i < totalComparadoresIndex; i++) {
    ///OUTBOUND
    camXCia[totalComparadoresIndex].outLead += camXCia[i].outLead;
    camXCia[totalComparadoresIndex].outNpc += camXCia[i].outNpc;
    camXCia[totalComparadoresIndex].outConv = trunc(
      calculaConversion(
        camXCia[totalComparadoresIndex].outLead,
        camXCia[totalComparadoresIndex].outNpc
      )
    );

    //INTERACCION TELEFONICA
    camXCia[totalComparadoresIndex].telLead += camXCia[i].telLead;
    camXCia[totalComparadoresIndex].telNpc += camXCia[i].telNpc;
    camXCia[totalComparadoresIndex].telConv = trunc(
      calculaConversion(
        camXCia[totalComparadoresIndex].telLead,
        camXCia[totalComparadoresIndex].telNpc
      )
    );

    ////INBOUND
    camXCia[totalComparadoresIndex].inLead += camXCia[i].inLead;
    camXCia[totalComparadoresIndex].inNpc += camXCia[i].inNpc;
    camXCia[totalComparadoresIndex].inConv = trunc(
      calculaConversion(
        camXCia[totalComparadoresIndex].inLead,
        camXCia[totalComparadoresIndex].inNpc
      )
    );

    ////INTERACCION ONLINE
    camXCia[totalComparadoresIndex].onlLead += camXCia[i].onlLead;
    camXCia[totalComparadoresIndex].onlNpc += camXCia[i].onlNpc;
    camXCia[totalComparadoresIndex].onlConv = trunc(
      calculaConversion(
        camXCia[totalComparadoresIndex].onlLead,
        camXCia[totalComparadoresIndex].onlNpc
      )
    );

    /////POLIZA COBRADA
    camXCia[totalComparadoresIndex].pCobrada += camXCia[i].pCobrada;
  }

  camXCia[totalComparadoresIndex].pCobrada = trunc(
    camXCia[totalComparadoresIndex].pCobrada,
    2
  );

  //A cada entrada le asignamos los valores generales
  camXCia[totalComparadoresIndex].gralLead =
    camXCia[totalComparadoresIndex].outLead +
    camXCia[totalComparadoresIndex].telLead +
    camXCia[totalComparadoresIndex].onlLead;
  camXCia[totalComparadoresIndex].gralNpc =
    camXCia[totalComparadoresIndex].outNpc +
    camXCia[totalComparadoresIndex].telNpc +
    camXCia[totalComparadoresIndex].onlNpc;
  camXCia[totalComparadoresIndex].gralConv = trunc(
    calculaConversion(
      camXCia[totalComparadoresIndex].gralLead,
      camXCia[totalComparadoresIndex].gralNpc
    )
  );
};

/**
 * Se hace la suma de totalMarcas y totalComparadores de la tabla, posteriormente
 * podemos hacer el calculo de los valores generales
 */
const calculaTotalVentaNueva = () => {
  let totalMarcasIndex = camXCia
    .map((row) => row.campanaGral)
    .indexOf("TOTAL MARCAS");

  let totalComparadoresIndex = camXCia
    .map((row) => row.campanaGral)
    .indexOf("TOTAL COMPARADORES");

  let totalVNIndex = camXCia
    .map((row) => row.campanaGral)
    .indexOf("TOTAL VENTA NUEVA");

  ///OUTBOUND
  camXCia[totalVNIndex].outLead =
    camXCia[totalMarcasIndex].outLead + camXCia[totalComparadoresIndex].outLead;
  camXCia[totalVNIndex].outNpc =
    camXCia[totalMarcasIndex].outNpc + camXCia[totalComparadoresIndex].outNpc;
  camXCia[totalVNIndex].outConv = trunc(
    calculaConversion(
      camXCia[totalVNIndex].outLead,
      camXCia[totalVNIndex].outNpc
    )
  );

  //INTERACCION TELEFONICA
  camXCia[totalVNIndex].telLead =
    camXCia[totalMarcasIndex].telLead + camXCia[totalComparadoresIndex].telLead;
  camXCia[totalVNIndex].telNpc =
    camXCia[totalMarcasIndex].telNpc + camXCia[totalComparadoresIndex].telNpc;
  camXCia[totalVNIndex].telConv = trunc(
    calculaConversion(
      camXCia[totalVNIndex].telLead,
      camXCia[totalVNIndex].telNpc
    )
  );

  ////INBOUND
  camXCia[totalVNIndex].inLead =
    camXCia[totalMarcasIndex].inLead + camXCia[totalComparadoresIndex].inLead;
  camXCia[totalVNIndex].inNpc =
    camXCia[totalMarcasIndex].inNpc + camXCia[totalComparadoresIndex].inNpc;
  camXCia[totalVNIndex].inConv = trunc(
    calculaConversion(camXCia[totalVNIndex].inLead, camXCia[totalVNIndex].inNpc)
  );

  ////INTERACCION ONLINE
  camXCia[totalVNIndex].onlLead =
    camXCia[totalMarcasIndex].onlLead + camXCia[totalComparadoresIndex].onlLead;
  camXCia[totalVNIndex].onlNpc =
    camXCia[totalMarcasIndex].onlNpc + camXCia[totalComparadoresIndex].onlNpc;
  camXCia[totalVNIndex].onlConv = trunc(
    calculaConversion(
      camXCia[totalVNIndex].onlLead,
      camXCia[totalVNIndex].onlNpc
    )
  );

  /////POLIZA COBRADA
  camXCia[totalVNIndex].pCobrada =
    camXCia[totalMarcasIndex].pCobrada +
    camXCia[totalComparadoresIndex].pCobrada;

  //A cada entrada le asignamos los valores generales
  camXCia[totalVNIndex].gralLead =
    camXCia[totalVNIndex].outLead +
    camXCia[totalVNIndex].telLead +
    camXCia[totalVNIndex].onlLead;
  camXCia[totalVNIndex].gralNpc =
    camXCia[totalVNIndex].outNpc +
    camXCia[totalVNIndex].telNpc +
    camXCia[totalVNIndex].onlNpc;
  camXCia[totalVNIndex].gralConv = trunc(
    calculaConversion(
      camXCia[totalVNIndex].gralLead,
      camXCia[totalVNIndex].gralNpc
    )
  );
};

/**
 * El total es la suma de MEJOR PRECIO, GMM, PERU y OTROS; de la tabla,
 * solo sacamos valores generales
 */
const calculaTotal = () => {
  let totalIndex = camXCia.map((row) => row.campanaGral).indexOf("TOTAL");

  for (let i = 27; i < totalIndex; i++) {
    ///OUTBOUND
    camXCia[totalIndex].outLead += camXCia[i].outLead;
    camXCia[totalIndex].outNpc += camXCia[i].outNpc;
    camXCia[totalIndex].outConv = trunc(
      calculaConversion(camXCia[totalIndex].outLead, camXCia[totalIndex].outNpc)
    );

    //INTERACCION TELEFONICA
    camXCia[totalIndex].telLead += camXCia[i].telLead;
    camXCia[totalIndex].telNpc += camXCia[i].telNpc;
    camXCia[totalIndex].telConv = trunc(
      calculaConversion(camXCia[totalIndex].telLead, camXCia[totalIndex].telNpc)
    );

    ////INBOUND
    camXCia[totalIndex].inLead += camXCia[i].inLead;
    camXCia[totalIndex].inNpc += camXCia[i].inNpc;
    camXCia[totalIndex].inConv = trunc(
      calculaConversion(camXCia[totalIndex].inLead, camXCia[totalIndex].inNpc)
    );

    ////INTERACCION ONLINE
    camXCia[totalIndex].onlLead += camXCia[i].onlLead;
    camXCia[totalIndex].onlNpc += camXCia[i].onlNpc;
    camXCia[totalIndex].onlConv = trunc(
      calculaConversion(camXCia[totalIndex].onlLead, camXCia[totalIndex].onlNpc)
    );

    //GENERALES
    camXCia[totalIndex].gralLead += camXCia[i].gralLead;
    camXCia[totalIndex].gralNpc += camXCia[i].gralNpc;
    camXCia[totalIndex].gralConv = trunc(
      calculaConversion(
        camXCia[totalIndex].gralLead,
        camXCia[totalIndex].gralNpc
      )
    );

    /////POLIZA COBRADA
    camXCia[totalIndex].pCobrada += camXCia[i].pCobrada;
  }

  camXCia[totalIndex].pCobrada = trunc(camXCia[totalIndex].pCobrada, 2);
};

/**
 * El total general viene de la suma de Total Venta Nueva, Cobranza y Retencion,
 * solo sacamos valores generales
 */
const calculaTotalGeneral = () => {
  let totalGeneralIndex = camXCia
    .map((row) => row.campanaGral)
    .indexOf("TOTAL GENERAL");
  let totalVentaNuevalIndex = camXCia
    .map((row) => row.campanaGral)
    .indexOf("TOTAL VENTA NUEVA");
  let totalIndex = camXCia.map((row) => row.campanaGral).indexOf("TOTAL");
  let cobranzaIndex = camXCia.map((row) => row.campanaGral).indexOf("COBRANZA");
  let retencionIndex = camXCia
    .map((row) => row.campanaGral)
    .indexOf("RETENCION");

  camXCia[totalGeneralIndex].gralLead =
    camXCia[totalVentaNuevalIndex].gralLead +
    camXCia[totalIndex].gralLead +
    camXCia[cobranzaIndex].gralLead +
    camXCia[retencionIndex].gralLead;

  camXCia[totalGeneralIndex].gralNpc =
    camXCia[totalVentaNuevalIndex].gralNpc +
    camXCia[totalIndex].gralNpc +
    camXCia[cobranzaIndex].gralNpc +
    camXCia[retencionIndex].gralNpc;

  camXCia[totalGeneralIndex].gralConv = trunc(
    calculaConversion(
      camXCia[totalGeneralIndex].gralLead,
      camXCia[totalGeneralIndex].gralNpc
    )
  );

  camXCia[totalGeneralIndex].pCobrada =
    camXCia[totalVentaNuevalIndex].pCobrada +
    camXCia[totalIndex].pCobrada +
    camXCia[cobranzaIndex].pCobrada +
    camXCia[retencionIndex].pCobrada;
};

//Inicializa variables a valores en 0
const reiniciaVariable = () => {
  for (let i = 0; i < filas.length; i++) {
    camXCia[i] = {
      campanaGral: filas[i],
      outLead: 0,
      outNpc: 0,
      outConv: 0,
      telLead: 0,
      telNpc: 0,
      telConv: 0,
      inLead: 0,
      inNpc: 0,
      inConv: 0,
      onlLead: 0,
      onlNpc: 0,
      onlConv: 0,
      gralLead: 0,
      gralNpc: 0,
      gralConv: 0,
      pCobrada: 0,
    };
  }
};

/**
 * Calcula la conversion de cada objeto tipo Tabla
 * @param contactosProd number
 * @param npc number
 * @returns number
 */
const calculaConversion = (contactosProd: number, npc: number) => {
  //Verificamos que la division se aposible; (No dividir entre 0)
  if (contactosProd > 0) {
    return (npc * 100) / contactosProd;
  } else {
    return 0;
  }
};

/**
 * Trunca el número recibido en las posiciones que recibimos como
 * argumento, si no se especifica las posiciones por default son 2
 * @param x number
 * @param posiciones number
 * @returns
 */
function trunc(x: number, posiciones = 2) {
  var s = x.toString();
  var l = s.length;
  var decimalLength = s.indexOf(".") + 1;
  var numStr = s.substr(0, decimalLength + posiciones);
  return Number(numStr);
}
