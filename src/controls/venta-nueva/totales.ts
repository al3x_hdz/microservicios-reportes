import { Board } from "../../database/models/ventaNueva/Board";

/**
 * A partir de un board calcula el subtotal {leads, npc, conversion}
 * @param board Board
 * @returns aux: Tabla
 */
export function calculaSubtotal(board: Board) {
  let aux = {
    leads: 0,
    ventas: 0,
    conversion: 0,
  };

  aux.leads =
    board.outbound.leads +
    board.interaccionOnline.leads +
    board.interaccionTelefonica.leads +
    board.inbound.leads;

  aux.ventas =
    board.outbound.npc +
    board.interaccionOnline.npc +
    board.interaccionTelefonica.npc +
    board.inbound.npc +
    board.mejorPrecio.npc +
    board.cobranza.npc +
    board.retencion.npc;

  aux.conversion = trunc(calculaConversion(aux.leads, aux.ventas));

  return aux;
}

/**
 * A partir de los subtotales, calcula el total general
 * @param subtotalVN Tabla
 * @param subtotalGMM Tabla
 * @param subtotalPeru Tabla
 * @returns Tabla
 */
export function generaTotalGeneral(
  subtotalVN: any,
  subtotalGMM: any,
  subtotalPeru: any
) {
  let aux = {
    leads: 0,
    ventas: 0,
    conversion: 0,
  };

  aux.leads = subtotalVN.leads + subtotalGMM.leads + subtotalPeru.leads;

  aux.ventas = subtotalVN.ventas + subtotalGMM.ventas + subtotalPeru.ventas;

  aux.conversion = trunc(calculaConversion(aux.leads, aux.ventas));

  return aux;
}

const calculaConversion = (contactosProd: number, npc: number) => {
  if (contactosProd > 0) {
    return (npc * 100) / contactosProd;
  } else {
    return 0;
  }
};

function trunc(x: number, posiciones = 3) {
  var s = x.toString();
  var l = s.length;
  var decimalLength = s.indexOf(".") + 1;
  var numStr = s.substr(0, decimalLength + posiciones);
  return Number(numStr);
}
