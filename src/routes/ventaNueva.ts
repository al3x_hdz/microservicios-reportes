import express from "express";
import VentaNuevaServices from "../services/VentaNuevaServices";
import { procesaDatosVN } from "../controls/venta-nueva/procesaVentaNueva";
("../controls/venta-nueva/procesaVentaNueva");
import { Campanna } from "../database/models/ventaNueva/Campanna";
import { procesaDatosGMM } from "../controls/venta-nueva/procesaGMM";
import { procesaDatosPeru } from "../controls/venta-nueva/procesaPeru";
import { generaTablaCamxCia } from "../controls/venta-nueva/camxcia";
import {
  calculaSubtotal,
  generaTotalGeneral,
} from "../controls/venta-nueva/totales";

const router = express.Router();

// Route --> /venta-nueva/fechaInicio/fechaFinal
router.get("/:fechaIn/:fechaFin", async (req, res) => {
  const fechaIn: string = req.params.fechaIn;
  const fechaFin: string = req.params.fechaFin;

  console.log("\n\n");

  let campannas: Campanna[] = [];
  let tipoSubareaDiferentes = [];

  //Solicitamos las tablas necesarias para el reporte
  let contactos: any = await VentaNuevaServices.getContactos(fechaIn, fechaFin);
  let prodDiaria: any = await VentaNuevaServices.getProdDiaria(
    fechaIn,
    fechaFin
  );
  let baseLlamadasEnt: any = await VentaNuevaServices.getBaseLlamadasEnt(
    fechaIn,
    fechaFin
  );

  console.log("INICIANDO PROCESO DE DATOS\n\n");
  //Extraemos todas las entradas del tipoSubarea que va a haber en la tabla Campaña
  //esto no inclulle ninguno que pertenezca a INBOUND
  //por que viene de otra tabla
  for (const iterator of contactos) {
    tipoSubareaDiferentes.push(iterator.Campana);
  }

  //Extraemos todas las entradas tipoSubarea que pertenecen a INBOUND y
  //los agregamos a a tipoSubareaDIferente
  for (const iterator of baseLlamadasEnt) {
    if (iterator.subAreaEmpleado) {
      if (iterator.subAreaEmpleado.includes(" IN ")) {
        tipoSubareaDiferentes.push(iterator.subAreaEmpleado);
      }
    }
  }

  //Eliminamos duplicados
  tipoSubareaDiferentes = [...new Set(tipoSubareaDiferentes)];

  //inicializamos los objetos del arreglo que va
  //a contener la tabla Campaña
  for (let iterator of tipoSubareaDiferentes) {
    campannas.push({
      tipoSubarea: iterator,
      compannia: generaCampanna(iterator),
      tipoContacto: generaTipoContacto(iterator),
      contactosProd: 0,
      npc: 0,
      conversion: 0,
      pCobrada: 0,
    });
  }

  //No INBOUND
  //Contactos Producidos
  for (let iterator of contactos) {
    for (let key in campannas) {
      if (iterator.Campana === campannas[key].tipoSubarea) {
        campannas[key].contactosProd++;
      }
    }
  }
  //NPC
  for (const iterator of prodDiaria) {
    for (let key in campannas) {
      if (iterator.tipoSubarea === campannas[key].tipoSubarea) {
        campannas[key].npc++;
      }
    }
  }
  //pCobrada
  for (const iterator of prodDiaria) {
    for (let key in campannas) {
      if (iterator.tipoSubarea === campannas[key].tipoSubarea) {
        campannas[key].pCobrada += iterator.cantidad;
      }
    }
  }

  //INBOUND
  //Llamadas
  for (let iterator of baseLlamadasEnt) {
    for (let key in campannas) {
      if (
        campannas[key].tipoContacto === "INBOUND" &&
        iterator.subAreaEmpleado === campannas[key].tipoSubarea
      ) {
        campannas[key].contactosProd++;
      }
    }
  }
  //NPC
  for (const iterator of prodDiaria) {
    for (let key in campannas) {
      if (
        campannas[key].tipoContacto === "INBOUND" &&
        iterator.tipoSubarea === campannas[key].tipoSubarea
      ) {
        campannas[key].npc++;
      }
    }
  }
  //pCobrada
  for (const iterator of prodDiaria) {
    for (let key in campannas) {
      if (
        campannas[key].tipoContacto === "INBOUND" &&
        iterator.tipoSubarea === campannas[key].tipoSubarea
      ) {
        campannas[key].pCobrada += iterator.cantidad;
      }
    }
  }

  //Generamos la conversión de todas las entradas y aprovechamos
  //la iteración para truncar los decimales de pCobrada y conversión
  for (const iterator of campannas) {
    iterator.conversion = iterator.npc / iterator.contactosProd;
    iterator.conversion = trunc(iterator.conversion);
    iterator.pCobrada = trunc(iterator.pCobrada);
  }

  /**
   * Separamos todas las campañas por venta-nueva- gastos médicos y peru
   */
  let ventaNueva = separaVentaNueva(campannas);
  let boardVentaNueva = procesaDatosVN(ventaNueva);

  let gastosMedicos = separaGMM(campannas);
  let boardGMM = procesaDatosGMM(gastosMedicos);

  let peru = separaPeru(campannas);
  let boardPeru = procesaDatosPeru(peru);

  /**
   * generamos el subtotal de
   * venta nueva en [0]
   * Gastos Médicos en [1]
   * Peru en [2]
   */
  let subtotal = [
    calculaSubtotal(boardVentaNueva),
    calculaSubtotal(boardGMM),
    calculaSubtotal(boardPeru),
  ];

  //El total general es el mismo para los tres
  let totalGeneral = generaTotalGeneral(subtotal[0], subtotal[1], subtotal[2]);

  /**
   * Generamos el descargable
   */
  let tabla = generaTablaCamxCia(
    campannas,
    subtotal[1],
    subtotal[2],
    boardVentaNueva.cobranza,
    boardVentaNueva.retencion
  );

  console.log("Enviando datos");

  //res.send(campannas);
  res.send({
    boardVentaNueva,
    boardGMM,
    boardPeru,
    tabla,
    subtotal,
    totalGeneral,
    campannas,
  });

  console.log("Datos enviados\n\n");
});

/**
 * Dependiendo del nombre del tipo de subarea
 * se retorna la compañia
 * @param tipoSub recibe un tipo de subarea
 * @returns string respectivo a compañia
 */
const generaCampanna = (tipoSub: string) => {
  if (tipoSub) {
    if (tipoSub.includes("MEJOR PRECIO")) {
      return "MEJOR PRECIO";
    }
    if (tipoSub.includes("QUALITAS UBER")) {
      return "UBER QUALITAS";
    }
    if (tipoSub.includes("PERU")) {
      return "PERU";
    }
    if (tipoSub.includes("ABA")) {
      return "ABA";
    }
    if (tipoSub.includes("AFIRME")) {
      return "AFIRME";
    }
    if (tipoSub.includes("AHORRA")) {
      return "AHORRA";
    }
    if (tipoSub.includes("ANA")) {
      return "ANA";
    }
    if (tipoSub.includes("ATLAS")) {
      return "ATLAS";
    }
    if (tipoSub.includes("BANORTE")) {
      return "BANORTE";
    }
    if (tipoSub.includes("COBRANZA")) {
      return "COBRANZA";
    }
    if (tipoSub.includes("COMPARADORES")) {
      return "COMPARA YA";
    }
    if (tipoSub.includes("CONTABILIDAD")) {
      return "CONTABILIDAD";
    }
    if (tipoSub.includes("EL AGUILA")) {
      return "EL AGUILA";
    }
    if (tipoSub.includes("EL POTOSI")) {
      return "EL POTOSI";
    }
    if (tipoSub.includes("GENERAL DE SEGUROS")) {
      return "GENERAL DE SEGUROS";
    }
    if (tipoSub.includes("GMM")) {
      return "GMM";
    }
    if (tipoSub.includes("GNP")) {
      return "GNP BRAND";
    }
    if (tipoSub.includes("HDI")) {
      return "HDI";
    }
    if (tipoSub.includes("INBURSA")) {
      return "INBURSA";
    }
    if (tipoSub.includes("LA LATINO")) {
      return "LA LATINO";
    }
    if (tipoSub.includes("MAPFRE")) {
      return "MAPFRE";
    }
    if (tipoSub.includes("MIGO")) {
      return "MIGO";
    }
    if (tipoSub.includes("MOTOS QUALITAS")) {
      return "MOTOS QUALITAS";
    }
    if (tipoSub.includes("MOTOS")) {
      return "MOTOS";
    }
    if (tipoSub.includes("MULTIMARCAS")) {
      return "MULTIMARCAS";
    }

    if (tipoSub.includes("QUALITAS")) {
      return "QUALITAS BRAND";
    }
    if (tipoSub.includes("RENOVACIONES") || tipoSub.includes("RN ")) {
      return "RENOVACIONES";
    }
    if (tipoSub.includes("RETENCION") || tipoSub.includes(" RET ")) {
      return "RETENCION";
    }
    if (tipoSub.includes("SUBSECUENTES")) {
      return "SUBSECUENTES";
    }
    if (tipoSub.includes("SURA")) {
      return "SURA";
    }
    if (tipoSub.includes("TAXIS")) {
      return "TAXIS";
    }
    if (
      tipoSub.includes("TRACTOCAMIONES") ||
      tipoSub.includes("TRACTO CAMIONES")
    ) {
      return "TRACTOCAMIONES";
    }
    if (tipoSub.includes("UBER")) {
      return "UBER";
    }
  }
  return "OTROS";
};

/**
 * Dependiendo del nombre del tipo de subarea
 * se retorna el tipo de contácto
 * @param tipoSub recibe un tipo de subarea
 * @returns string respectivo a tipo de contacto
 */
const generaTipoContacto = (tipoSub: string) => {
  if (tipoSub) {
    if (tipoSub.includes(" MEJOR PRECIO") && tipoSub.includes("GMM ")) {
      return "GASTOS MEDICOS MAYORES";
    }
    if (tipoSub.includes(" MEJOR PRECIO")) {
      return "MEJOR PRECIO";
    }
    if (tipoSub.includes("RETENCION")) {
      return "RETENCION";
    }
    if (tipoSub.includes("COBRANZA")) {
      return "COBRANZA";
    }
    if (tipoSub.includes(" IN ")) {
      return "INBOUND";
    }
    if (
      tipoSub.includes(" OUT") ||
      tipoSub.includes("VN QUALITAS BRAN") ||
      tipoSub.includes("VN GNP BRAN") ||
      tipoSub.includes("VN ANA BRAND") ||
      tipoSub.includes("VN ANA")
    ) {
      return "OUTBOUND";
    }
    if (tipoSub.includes("ONLINE") || tipoSub.includes("E-COMM")) {
      return "INTERACCION ONLINE";
    }
    if (
      tipoSub.includes("INT TEL") ||
      tipoSub.includes("VN AHORRA INTERACCIÓN")
    ) {
      return "INTERACCION TELEFONICA";
    }

    if (tipoSub.includes("GMM")) {
      return "GASTOS MEDICOS MAYORES";
    }
    if (tipoSub.includes("PERU")) {
      return "PERU";
    }
    if (tipoSub.includes("CONTABILIDAD")) {
      return "CONTABILIDAD";
    }
  }
  return "OTROS";
};

/**
 * Va a separar todas las campañas que no pertenecen a GMM o Peru
 * a partit del tipoContacto de cada campaña
 * @param campannas arreglo tipo Campanna
 * @returns arreglo tipo Campanna
 */
const separaVentaNueva = (campannas: Campanna[]): Campanna[] => {
  let aux: Campanna[] = [];
  campannas.map((campanna) => {
    if (
      campanna.tipoContacto !== "GASTOS MEDICOS MAYORES" &&
      campanna.tipoContacto !== "PERU"
    ) {
      aux.push(campanna);
    }
  });
  return aux;
};

/**
 * Extraemos las campañas que en tipoContacto perteneces
 * a GMM
 * @param @param campannas arreglo tipo Campanna
 * @returns arreglo tipo Campanna
 */
const separaGMM = (campannas: Campanna[]): Campanna[] => {
  let aux: Campanna[] = [];
  campannas.map((campanna) => {
    if (
      campanna.tipoContacto === "GASTOS MEDICOS MAYORES" ||
      campanna.compannia === "GMM"
    ) {
      aux.push(campanna);
    }
  });
  return aux;
};

/**
 * Extraemos las campañas que en tipoContacto perteneces
 * a Peru
 * @param @param campannas arreglo tipo Campanna
 * @returns arreglo tipo Campanna
 */
const separaPeru = (campannas: Campanna[]): Campanna[] => {
  let aux: Campanna[] = [];
  campannas.map((campanna) => {
    if (campanna.tipoContacto === "PERU" || campanna.compannia === "PERU") {
      aux.push(campanna);
    }
  });
  return aux;
};

/**
 * Trunca los decimales de un número recibido
 * @param x numero que deseas convertir
 * @param posiciones de decimales a los que deseamos truncar
 * @returns resultado tipo number
 */
function trunc(x: number, posiciones = 2) {
  var s = x.toString();
  var l = s.length;
  var decimalLength = s.indexOf(".") + 1;
  var numStr = s.substr(0, decimalLength + posiciones);
  return Number(numStr);
}

export default router;
