import express from "express";

import LlamadasEntrantesReporteServices from "../services/LlamadasEntrantesReporteServices";

const router = express.Router();

// Route --> /llamadas-entrantes
router.get("/", async (req, res) => {
  let reportes = await LlamadasEntrantesReporteServices.getAll();
  res.send(reportes);
});

// Route --> /llamadas-entrantes/:month/:year
router.get("/:month/:year", async (req, res) => {
  const month = parseInt(req.params.month);
  const year = parseInt(req.params.year);
  let reportes = await LlamadasEntrantesReporteServices.getByMonth(month, year);
  res.send(reportes);
});

export default router;
