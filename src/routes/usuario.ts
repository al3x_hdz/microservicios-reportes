import express from "express";
import UsuariosService from "../services/UsuariosServices";
import { googleVerify } from "../helpers/google-verify";
const router = express.Router();

// Route --> /google/login
router.post("/google/login", async (req, res) => {
  let acceso;
  let picture;

  await googleVerify(req.body.token).then(async (dato: any) => {
    try {
      let querryResult: any = await UsuariosService.getUser(dato.email);
      if (querryResult) {
        acceso = true;
        picture = dato.picture;
        return res.status(201).send({ acceso, picture });
      } else {
        acceso = false;
        return res.status(201).send({ acceso });
      }
    } catch (error) {
      return res.status(401).send(error);
    }
  });
});

export default router;
