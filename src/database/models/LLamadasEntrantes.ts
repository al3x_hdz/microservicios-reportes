import { sequelize } from "../db";
import { DataTypes, Model } from "sequelize";

class LLamadasEntrantes extends Model {
  //Model type definition

  public fechaGen!: string;
  public estatus!: string;
  public event!: string;
  public calldate!: Date;
  public uniqueid!: string;
  public opcionReal!: string;
  public aplicacion!: string;
  public campanaIVR!: string;
  public turnoCDR!: string;
  public fechaRegistro!: Date;
  public ClasificaciónLlamadas!: string;
  public idLlamada!: number;
  public idAsteriks!: string;
  //public tipoLlamada!: string;
  public telefonoCliente!: string;
  public telefonoMarcado!: string;
  public turno!: string;
  public queue!: string;
  public campana!: string;
  public area!: string;
  public idEmpleado!: number;
  public nombreEmpleado!: string;
  public subAreaEmpleado!: string;
  public fechaInicio!: Date;
  public fechaFinal!: Date;
  public duracionLlamada!: string;
  public atencionLlamada!: string;
  public estadoLlamada!: string;
  public subetiquetaSolicitud!: string;
  public idSolicitud!: number;
  public areaIVR!: string;
}

// LlamadasEntrantes initialization
// this part relates the table
// with a objetc model to use
// the sequelize functions

LLamadasEntrantes.init(
  {
    fechaGen: {
      type: DataTypes.DATE,
    },
    estatus: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    event: {
      type: DataTypes.STRING,
    },
    calldate: {
      type: DataTypes.DATE,
      primaryKey: true,
    },
    uniqueId: {
      type: DataTypes.STRING,
    },
    opcionReal: {
      type: DataTypes.STRING,
    },
    aplicacion: {
      type: DataTypes.STRING,
    },
    campanaIVR: {
      type: DataTypes.STRING,
    },
    turnoCDR: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    fechaRegistro: {
      type: DataTypes.DATE,
    },
    ClasificaciónLlamadas: {
      type: DataTypes.STRING,
    },
    idLlamada: {
      type: DataTypes.INTEGER,
    },
    idAsterisk: {
      type: DataTypes.STRING,
    },
    // tipoLlamada: {
    //   type: DataTypes.STRING,
    // },
    telefonoCliente: {
      type: DataTypes.STRING,
    },
    telefonoMarcado: {
      type: DataTypes.STRING,
    },
    turno: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    queue: {
      type: DataTypes.STRING,
    },
    campana: {
      type: DataTypes.STRING,
    },
    area: {
      type: DataTypes.STRING,
    },
    idEmpleado: {
      type: DataTypes.BIGINT,
    },
    nombreEmpleado: {
      type: DataTypes.STRING,
    },
    subAreaEmpleado: {
      type: DataTypes.STRING,
    },
    fechaInicio: {
      type: DataTypes.DATE,
    },
    fechaFinal: {
      type: DataTypes.DATE,
    },
    duracionLlamada: {
      type: DataTypes.STRING,
    },
    atencionLlamada: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    estadoLlamada: {
      type: DataTypes.STRING,
    },
    subetiquetaSolicitud: {
      type: DataTypes.STRING,
    },
    idSolicitud: {
      type: DataTypes.BIGINT,
    },
    areaIVR: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    tableName: "llamadasEnT2020",
    timestamps: false,
    sequelize,
  }
);

export default LLamadasEntrantes;
