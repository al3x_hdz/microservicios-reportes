import { Tabla } from "./Tabla";

export interface Board {
  outbound: Tabla;
  interaccionOnline: Tabla;
  interaccionTelefonica: Tabla;
  inbound: Tabla;
  otros: Tabla;
  mejorPrecio: Tabla;
  cobranza: Tabla;
  retencion: Tabla;
}
