import { sequelize } from "../db";
import { DataTypes, Model } from "sequelize";

class Usuario extends Model {}

Usuario.init(
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    idCorreo: {
      type: DataTypes.INTEGER,
    },
    idGrupo: {
      type: DataTypes.INTEGER,
    },
    idTipo: {
      type: DataTypes.INTEGER,
    },
    idSubarea: {
      type: DataTypes.INTEGER,
    },
    usuario: {
      type: DataTypes.STRING,
    },
    password: {
      type: DataTypes.STRING,
    },
    fechaCreacion: {
      type: DataTypes.DATE,
    },
    estado: {
      type: DataTypes.INTEGER,
    },
    imagenURL: {
      type: DataTypes.STRING,
    },
    nombre: {
      type: DataTypes.STRING,
    },
    extension: {
      type: DataTypes.STRING,
    },
  },
  {
    tableName: "usuarioView",
    timestamps: false,
    sequelize,
  }
);

export default Usuario;
