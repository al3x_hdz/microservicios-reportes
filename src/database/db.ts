import { Sequelize } from "sequelize";
import dotenv from "dotenv";

dotenv.config();

export const sequelize = new Sequelize(
  process.env.DB as string,
  process.env.DB_USERNAME as string,
  process.env.DB_PASSWORD as string,
  {
    host: process.env.HOST as string,
    dialect: "mysql",
  }
);

/**
 * Create the conection with the database,
 * the function uses the configuration
 * defined in the config.ts file at
 * the /src dir
 *
 * if the conection was refused, it going to
 * show a error at the console logs
 */
export function connectDb(): void {
  sequelize
    .authenticate()
    .then(() => {
      console.log("\n\n---------> Database Conection Succefull!\n\n");
    })
    .catch((error) => {
      console.log("\n\n---------> ERROR - Database Conection Failed!\n\n");
      console.error(error);
    });
}

/**
 * Finish the database conection
 */
export function closeDb(): void {
  sequelize.close().then(() => {
    console.log("\n\n---------> Database Conection Closed Successfull!\n\n");
  });
}
