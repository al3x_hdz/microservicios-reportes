import express from "express";
import cors from "cors";
import { connectDb } from "./database/db";
import dotenv from "dotenv";

import routerLlamadasEntrantes from "./routes/llamadasEntrantes";
import routerUsuarios from "./routes/usuario";
import routerVentaNueva from "./routes/ventaNueva";

//app init
const app = express();

//middleware
app.use(cors());
app.use(express.json());
dotenv.config();

//setings
const PORT = process.env.PORT;

//routes
app.use("/llamadas-entrantes", routerLlamadasEntrantes);
app.use("/usuarios", routerUsuarios);
app.use("/venta-nueva", routerVentaNueva);

//DB conection
connectDb();

//service initialzation
app.listen(PORT, () => {
  console.log(`\n\nhttp://localhost:${PORT}\n\n`);
});
